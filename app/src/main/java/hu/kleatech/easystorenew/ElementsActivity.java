package hu.kleatech.easystorenew;

import android.app.ActionBar;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.adapter.ElementsRecyclerAdapter;
import hu.kleatech.easystorenew.app.MainApplication;
import hu.kleatech.easystorenew.databinding.ActivityElementsBinding;
import hu.kleatech.easystorenew.listener.RecyclerItemClickListener;
import hu.kleatech.easystorenew.model.Element;
import hu.kleatech.easystorenew.service.ElementService;
import hu.kleatech.easystorenew.utils.GlobalState;
import hu.kleatech.easystorenew.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;
import static hu.kleatech.easystorenew.utils.Utils.tryThis;

public class ElementsActivity extends BaseActivity {

    public static final String HAVE_CONTEXT = "h";
    public static final String COMP_SERIAL = "c";

    private final Helpers HELPERS = new Helpers();

    @BindView(R.id.elements_edittext_query)         protected EditText query;
    @BindView(R.id.elements_progressbar)            protected ProgressBar progressBar;
    @BindView(R.id.elements_recyclerlayout_results) protected RecyclerView results;
    @BindView(R.id.elements_spinner_field)          protected Spinner field;
    @BindView(R.id.elements_scrollview)             protected ScrollView scrollView;
    @BindView(R.id.elements_constraintlayout_search)protected ConstraintLayout search;

    private ElementService elementService;
    private ElementsRecyclerAdapter elementsRecyclerAdapter;
    private ActivityElementsBinding binding;
    private Unbinder unbinder;
    private boolean haveContext;
    private String compSerial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_elements);
        unbinder = ButterKnife.bind(this);
        haveContext = getIntent().getBooleanExtra(HAVE_CONTEXT, false);
        compSerial = getIntent().getStringExtra(COMP_SERIAL);
        if (haveContext) {
            search.setVisibility(GONE);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.custom_action_bar);
            View actionbar = getSupportActionBar().getCustomView();
            TextView textViewTitle = actionbar.findViewById(R.id.customActionBar_textview_title);
            textViewTitle.setText(getTitle());
            View btn = actionbar.findViewById(R.id.fab_new);
            btn.setOnClickListener(b -> {
                Intent intent = new Intent(this, ElementDetailsActivity.class);
                intent.putExtra(ElementDetailsActivity.EDITING, false);
                intent.putExtra(ElementDetailsActivity.COMP_SERIAL, compSerial);
                startActivity(intent);
            });
        }

        results.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL));
        results.setLayoutManager(new LinearLayoutManager(this));
        results.setNestedScrollingEnabled(false);
        results.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {
            Intent intent = new Intent(this, ElementDetailsActivity.class);
            intent.putExtra(ElementDetailsActivity.SERIAL, HELPERS.getApplication().getElements().get(position).getSerial());
            intent.putExtra(ElementDetailsActivity.COMP_SERIAL, HELPERS.getApplication().getElements().get(position).getCompartmentSerial());
            intent.putExtra(ElementDetailsActivity.EDITING, true);
            startActivity(intent);
        }));
        nullsafe(() -> elementService = GlobalState.retrofit.create(ElementService.class));

        ArrayAdapter temp = ArrayAdapter.createFromResource(this,
                R.array.elem_search_criterias,
                android.R.layout.simple_spinner_item);
        temp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        field.setAdapter(temp);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!haveContext) finish();
    }

    @OnClick(R.id.elements_fab_up)
    protected void scrollUp() {
        scrollView.smoothScrollTo(0,0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (haveContext) {
            HELPERS.hideKeyboard();
            HELPERS.displayProgress(true);
            elementService.all(compSerial, 0, 10, "1")
                    .enqueue(new Callback<List<Element>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Element>> call, @NonNull Response<List<Element>> response) {
                            HELPERS.displayResults(response.body());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Element>> call, @NonNull Throwable t) {
                            HELPERS.displayError(t);
                        }
                    });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    @OnClick(R.id.elements_button_search)
    public void search() {
        String queryStr = this.query.getText().toString().trim();
        if(TextUtils.isEmpty(queryStr)) {
            query.setError("The query cannot be empty");
        }
        else {
            HELPERS.hideKeyboard();
            HELPERS.displayProgress(true);
            elementService.search(ImmutableMap.of(
                    field.getSelectedItem().toString().toLowerCase(), queryStr,
                    "requestImages", "1"))
                    .enqueue(new Callback<List<Element>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Element>> call, @NonNull Response<List<Element>> response) {
                            HELPERS.displayResults(response.body());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Element>> call, @NonNull Throwable t) {
                            HELPERS.displayError(t);
                        }
                    });
        }
    }

    private class Helpers {
        MainApplication getApplication() { return (MainApplication)ElementsActivity.this.getApplication(); }
        void hideKeyboard() {
            InputMethodManager imm = (InputMethodManager)ElementsActivity.this.getSystemService(INPUT_METHOD_SERVICE);
            final Utils.Mutable<View> view = Mutable(ElementsActivity.this.getCurrentFocus());
            if (view.obj != null) view.obj = new View(ElementsActivity.this);
            nullsafe(() -> imm.hideSoftInputFromWindow(view.obj.getWindowToken(), 0));
        }
        void displayProgress(boolean show) {
            results.setVisibility(show?GONE:VISIBLE);
            progressBar.setVisibility(show?VISIBLE:GONE);
        }
        void displayResults(List<Element> results) {
            if (results == null) results = new LinkedList<>();
            displayProgress(false);
            if (elementsRecyclerAdapter == null) {
                elementsRecyclerAdapter = new ElementsRecyclerAdapter(results);
                ElementsActivity.this.results.setAdapter(elementsRecyclerAdapter);
                getApplication().setElements(results);
            }
            else {
                getApplication().getElements().clear();
                getApplication().getElements().addAll(results);
                elementsRecyclerAdapter.notifyDataSetChanged();
            }
        }
        //TODO refactor to central implementation
        void displayError(Throwable t) {
            displayProgress(false);
            if (getApplication().getElements() != null) {
                getApplication().getElements().clear();
                if (elementsRecyclerAdapter != null) {
                    elementsRecyclerAdapter.notifyDataSetChanged();
                }
                Log.w("Compartments", t.getMessage());
                Toast.makeText(ElementsActivity.this, "ERROR: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
