package hu.kleatech.easystorenew.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import hu.kleatech.easystorenew.BR;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
public class Compartment extends BaseObservable implements Dto {
    private String serial;
    private String name;
    private String room;
    private String shelf;
    private List<String> imagesBase64;
    private Properties infos;
    private Set<String> tags;

    public static Compartment newEmpty() {
        return Compartment.builder().imagesBase64(new LinkedList<>()).build();
    }

    @Override
    @Bindable public String getSerial() { return serial; }
    public void setSerial(String serial) {
        if (!Objects.equals(this.serial, serial)) this.serial = serial;
        notifyPropertyChanged(BR.serial);
    }

    @Bindable public String getName() { return name; }
    public void setName(String name) {
        if (!Objects.equals(this.name, name)) this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable public String getRoom() { return room; }
    public void setRoom(String room) {
        if (!Objects.equals(this.room, room)) this.room = room;
        notifyPropertyChanged(BR.room);
    }

    @Bindable public String getShelf() { return shelf; }
    public void setShelf(String shelf) {
        if (!Objects.equals(this.shelf, shelf)) this.shelf = shelf;
        notifyPropertyChanged(BR.shelf);
    }

    @Override
    @Bindable public List<String> getImagesBase64() { return imagesBase64; }
    public void setImagesBase64(List<String> imagesBase64) {
        if (!Objects.equals(this.imagesBase64, imagesBase64)) this.imagesBase64 = imagesBase64;
        notifyPropertyChanged(BR.imagesBase64);
    }

    @Bindable public Properties getInfos() { return infos; }
    public void setInfos(Properties infos) {
        if (!Objects.equals(this.infos, infos)) this.infos = infos;
        notifyPropertyChanged(BR.infos);
    }

    @Bindable public Set<String> getTags() { return tags; }
    public void setTags(Set<String> tags) {
        if (!Objects.equals(this.tags, tags)) this.tags = tags;
        notifyPropertyChanged(BR.tags);
    }
}