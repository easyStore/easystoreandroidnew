package hu.kleatech.easystorenew.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import hu.kleatech.easystorenew.BR;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
public class Element extends BaseObservable implements Dto {
    private String name;
    private Double value;
    private String desc;
    private String status;
    private String serial;
    private String compartmentSerial;
    private Properties infos;
    private List<String> imagesBase64;
    private Set<String> tags;

    public static Element newEmpty(String compSerial) {
        return Element.builder()
                .compartmentSerial(compSerial)
                .imagesBase64(new LinkedList<>())
                .value(0.0).build();
    }

    @Override
    @Bindable
    public String getSerial() { return serial; }
    public void setSerial(String serial) {
        if (!Objects.equals(this.serial, serial)) this.serial = serial;
        notifyPropertyChanged(BR.serial);
    }

    @Bindable public String getName() { return name; }
    public void setName(String name) {
        if (!Objects.equals(this.name, name)) this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Override
    @Bindable public List<String> getImagesBase64() { return imagesBase64; }
    public void setImagesBase64(List<String> imagesBase64) {
        if (!Objects.equals(this.imagesBase64, imagesBase64)) this.imagesBase64 = imagesBase64;
        notifyPropertyChanged(BR.imagesBase64);
    }

    @Bindable public Properties getInfos() { return infos; }
    public void setInfos(Properties infos) {
        if (!Objects.equals(this.infos, infos)) this.infos = infos;
        notifyPropertyChanged(BR.infos);
    }

    @Bindable public Set<String> getTags() { return tags; }
    public void setTags(Set<String> tags) {
        if (!Objects.equals(this.tags, tags)) this.tags = tags;
        notifyPropertyChanged(BR.tags);
    }

    @Bindable public Double getValue() { return value; }
    public void setValue(Double value) {
        if (!Objects.equals(this.value, value)) this.value = value;
        notifyPropertyChanged(BR.value);
    }

    @Bindable public String getDesc() { return desc; }
    public void setDesc(String desc) {
        if (!Objects.equals(this.desc, desc)) this.desc = desc;
        notifyPropertyChanged(BR.desc);
    }

    @Bindable public String getStatus() { return status; }
    public void setStatus(String status) {
        if (!Objects.equals(this.status, status)) this.status = status;
        notifyPropertyChanged(BR.status);
    }

    @Bindable public String getCompartmentSerial() { return compartmentSerial; }
    public void setCompartmentSerial(String compartmentSerial) {
        if (!Objects.equals(this.compartmentSerial, compartmentSerial)) this.compartmentSerial = compartmentSerial;
        notifyPropertyChanged(BR.compartmentSerial);
    }
}
