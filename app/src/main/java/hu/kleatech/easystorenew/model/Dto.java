package hu.kleatech.easystorenew.model;

import java.util.List;

interface Dto {
    String getSerial();
    List<String> getImagesBase64();
}
