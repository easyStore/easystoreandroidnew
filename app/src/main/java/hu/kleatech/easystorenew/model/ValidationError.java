package hu.kleatech.easystorenew.model;

import lombok.Data;

@Data
public class ValidationError {
    private String field;
    private String message;
}
