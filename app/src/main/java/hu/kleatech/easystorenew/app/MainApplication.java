package hu.kleatech.easystorenew.app;

import android.app.Application;

import java.util.List;

import hu.kleatech.easystorenew.model.Compartment;
import hu.kleatech.easystorenew.model.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MainApplication extends Application {
    private List<Compartment> compartments;
    private List<Element> elements;
}
