package hu.kleatech.easystorenew;

import android.app.ActionBar;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.adapter.CompartmentsRecyclerAdapter;
import hu.kleatech.easystorenew.app.MainApplication;
import hu.kleatech.easystorenew.databinding.ActivityCompartmentsBinding;
import hu.kleatech.easystorenew.listener.RecyclerItemClickListener;
import hu.kleatech.easystorenew.model.Compartment;
import hu.kleatech.easystorenew.service.CompartmentService;
import hu.kleatech.easystorenew.utils.GlobalState;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static hu.kleatech.easystorenew.utils.Utils.*;
import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;

public class CompartmentsActivity extends BaseActivity {

    private final Helpers HELPERS = new Helpers();

    @BindView(R.id.compartments_edittext_query)         protected EditText query;
    @BindView(R.id.compartments_progressbar)            protected ProgressBar progressBar;
    @BindView(R.id.compartments_recyclerlayout_results) protected RecyclerView results;
    @BindView(R.id.compartments_spinner_field)          protected Spinner field;
    @BindView(R.id.compartments_scrollview)             protected ScrollView scrollView;

    private CompartmentService compartmentService;
    private CompartmentsRecyclerAdapter compartmentsRecyclerAdapter;
    private ActivityCompartmentsBinding binding;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_compartments);
        unbinder = ButterKnife.bind(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        View actionbar = getSupportActionBar().getCustomView();
        View btn = actionbar.findViewById(R.id.fab_new);
        btn.setOnClickListener(b -> {
            Intent intent = new Intent(this, CompartmentDetailsActivity.class);
            intent.putExtra(CompartmentDetailsActivity.EDITING, false);
            startActivity(intent);
        });

        results.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL));
        results.setLayoutManager(new LinearLayoutManager(this));
        results.setNestedScrollingEnabled(false);
        results.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {
            Intent intent = new Intent(this, CompartmentDetailsActivity.class);
            intent.putExtra(CompartmentDetailsActivity.SERIAL, HELPERS.getApplication().getCompartments().get(position).getSerial());
            intent.putExtra(CompartmentDetailsActivity.EDITING, true);
            startActivity(intent);
        }));
        nullsafe(() -> compartmentService = GlobalState.retrofit.create(CompartmentService.class));

        ArrayAdapter temp = ArrayAdapter.createFromResource(this,
                R.array.comp_search_criterias,
                android.R.layout.simple_spinner_item);
        temp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        field.setAdapter(temp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HELPERS.hideKeyboard();
        HELPERS.displayProgress(true);
        compartmentService.all(0, 10, "1")
                .enqueue(new Callback<List<Compartment>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<Compartment>> call, @NonNull Response<List<Compartment>> response) {
                        HELPERS.displayResults(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Compartment>> call, @NonNull Throwable t) {
                        HELPERS.displayError(t);
                    }
                });
        HELPERS.hideKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    @OnClick(R.id.compartments_button_search)
    public void onSearchButtonClicked() {
        String queryStr = this.query.getText().toString().trim();
        if(TextUtils.isEmpty(queryStr)) {
            query.setError("The query cannot be empty");
        }
        else {
            HELPERS.hideKeyboard();
            HELPERS.displayProgress(true);
            compartmentService.search(ImmutableMap.of(
                        field.getSelectedItem().toString().toLowerCase(), queryStr,
                        "requestImages", "1"))
                    .enqueue(new Callback<List<Compartment>>() {
                @Override
                public void onResponse(@NonNull Call<List<Compartment>> call, @NonNull Response<List<Compartment>> response) {
                    HELPERS.displayResults(response.body());
                }

                @Override
                public void onFailure(@NonNull Call<List<Compartment>> call, @NonNull Throwable t) {
                    HELPERS.displayError(t);
                }
            });
        }
    }

    @OnClick(R.id.compartments_fab_up)
    protected void scrollUp() {
        scrollView.smoothScrollTo(0,0);
    }

    @OnClick(R.id.compartments_fab_elements)
    public void elements() {
        Intent intent = new Intent(this, ElementsActivity.class);
        intent.putExtra(ElementsActivity.HAVE_CONTEXT, false);
        startActivity(intent);
    }

    private class Helpers {
        MainApplication getApplication() {
            return ((MainApplication)CompartmentsActivity.this.getApplication());
        }
        void hideKeyboard() {
            InputMethodManager imm = (InputMethodManager)CompartmentsActivity.this.getSystemService(INPUT_METHOD_SERVICE);
            final Mutable<View> view = Mutable(CompartmentsActivity.this.getCurrentFocus());
            if (view.obj != null) view.obj = new View(CompartmentsActivity.this);
            nullsafe(() -> imm.hideSoftInputFromWindow(view.obj.getWindowToken(), 0));
        }
        void displayProgress(boolean show) {
            results.setVisibility(show?GONE:VISIBLE);
            progressBar.setVisibility(show?VISIBLE:GONE);
        }
        void displayResults(List<Compartment> results) {
            if (results == null) results = new LinkedList<>();
            displayProgress(false);
            if (compartmentsRecyclerAdapter == null) {
                compartmentsRecyclerAdapter = new CompartmentsRecyclerAdapter(results);
                CompartmentsActivity.this.results.setAdapter(compartmentsRecyclerAdapter);
                getApplication().setCompartments(results);
            }
            else {
                getApplication().getCompartments().clear();
                getApplication().getCompartments().addAll(results);
                compartmentsRecyclerAdapter.notifyDataSetChanged();
            }
        }
        //TODO refactor to central implementation
        void displayError(Throwable t) {
            displayProgress(false);
            if (getApplication().getCompartments() != null) {
                getApplication().getCompartments().clear();
                if (compartmentsRecyclerAdapter != null) {
                    compartmentsRecyclerAdapter.notifyDataSetChanged();
                }
                Log.w("Compartments", t.getMessage());
                Toast.makeText(CompartmentsActivity.this, "ERROR: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
