package hu.kleatech.easystorenew.service;

import java.util.List;
import java.util.Map;

import hu.kleatech.easystorenew.model.Compartment;
import hu.kleatech.easystorenew.model.ValidationError;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface CompartmentService {
    @GET("compartment/query")
    Call<List<Compartment>> search(@QueryMap Map<String, String> map);

    @GET("compartment/all")
    Call<List<Compartment>> all(@Query("page") int page, @Query("pageSize") int pageSize, @Query("requestImages") String requestImages);

    @GET("compartment")
    Call<Compartment> get(@Query("serial") String serial);

    @POST("compartment/add")
    Call<List<ValidationError>> add(@Body Compartment compartment);

    @POST("compartment/save")
    Call<List<ValidationError>> save(@Body Compartment compartment);

    @GET("compartment/delete")
    Call<String> delete(@Query("serial") String serial);
}
