package hu.kleatech.easystorenew.service;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.util.SparseArray;
import android.view.TextureView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;

import io.github.silvaren.easyrs.tools.Nv21Image;
import lombok.Getter;

import static hu.kleatech.easystorenew.utils.Utils.CommandWithArgument;
import static hu.kleatech.easystorenew.utils.Utils.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.async;
import static hu.kleatech.easystorenew.utils.Utils.delay;
import static hu.kleatech.easystorenew.utils.Utils.either;
import static hu.kleatech.easystorenew.utils.Utils.ignoreEx;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;

//Magic class
@SuppressWarnings("deprecation")
public class QRCamera {

    public static final String DETECTOR_NOT_OPERATIONAL = "Detector is not operational.";

    private final TextureView textureView;
    private Camera camera;
    private int previewFormat;
    private BarcodeDetector detector;
    private AtomicBoolean capture;
    private Barcode foundOne = null;
    private boolean rsAvailable = true;
    @Getter private String rawQR;
    private RenderScript rs;
    private Runnable onFoundOneListener;
    private boolean flash = false;
    private int interval;

    public QRCamera(TextureView textureView, Activity caller) throws RuntimeException {
        ignoreEx(() -> rs = RenderScript.create(caller.getApplicationContext()));
        capture = new AtomicBoolean(false);
        this.textureView = textureView;
        detector = new BarcodeDetector.Builder(caller.getApplicationContext())
                .setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            throw new RuntimeException(DETECTOR_NOT_OPERATIONAL);
        }
    }

    public void setOnFoundOneListener(Runnable onFoundOneListener) {
        this.onFoundOneListener = onFoundOneListener;
    }

    public void startPreview() {
        ignoreEx(() -> camera = Camera.open());
        Camera.Parameters params = camera.getParameters();
        previewFormat = params.getPreviewFormat();
        //for (Camera.Size size : params.getSupportedPreviewSizes()) Log.e("Supported size", size.width + "x" + size.height);
        params.setPreviewSize(640, 480);
        if (flash) params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(params);
        camera.setDisplayOrientation(90);
        if (!textureView.isAvailable()) textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                ignoreEx(() -> camera.setPreviewTexture(surface));
                camera.startPreview();
            }
            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {}
            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) { return true; }
            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {}
        });
        else {
            ignoreEx(() -> camera.setPreviewTexture(textureView.getSurfaceTexture()));
            camera.startPreview();
        }
    }

    public void turnOnFlash() {
        flash = true;
        async(() -> ignoreEx(() -> {
            boolean cap = capture.get();
            stopPreview();
            startPreview();
            if (cap) startCapture(interval);
        }));
    }

    public void stopPreview() {
        stopCapture();
        nullsafe(() -> camera.release());
        camera = null;
    }

    public void startCapture(int interval) {
        this.interval = interval;
        @SuppressWarnings("WeakerAccess")
        class capturer {
            Bitmap bitmap;
            Frame frame;
            SparseArray<Barcode> barcodes;
            void run() throws Exception {
                while (capture.get()) {
                    camera.setOneShotPreviewCallback((bytes, camera) -> ignoreEx(() -> {
                        int width = camera.getParameters().getPreviewSize().width;
                        int height = camera.getParameters().getPreviewSize().height;
                        bitmap = previewToBitmap(bytes, previewFormat, width, height);
                        frame = new Frame.Builder().setBitmap(bitmap).build();
                        barcodes = detector.detect(frame);
                        try {
                            foundOne = barcodes.valueAt(0);
                            Log.i("Result", foundOne.rawValue);
                            rawQR = foundOne.rawValue;
                            onFoundOneListener.run();
                        }
                        catch (ArrayIndexOutOfBoundsException e) {
                            if (!isFound()) {
                                Log.i("Result", "No QR code found.");
                            }
                        }
                    }));
                    delay(interval);
                }
            }
        }
        capture.set(true);
        async(() -> ignoreEx(() -> new capturer().run()));
    }

    public void takeSnapshot(CommandWithArgument<Bitmap> onCaptureListener) throws RuntimeException {
        camera.setOneShotPreviewCallback(((bytes, camera1) -> {
            int width = camera.getParameters().getPreviewSize().width;
            int height = camera.getParameters().getPreviewSize().height;
            Mutable<Bitmap> bm = Mutable(previewToBitmap(bytes, previewFormat ,width, height));
            Log.i("Snapshot taken", bm.obj==null?"null":"OK");
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bm.obj = Bitmap.createBitmap(bm.obj, 140, 0, 360, 480, matrix, true);
            ignoreEx(() -> onCaptureListener.execute(bm.obj));
        }));
    }

    private Bitmap previewToBitmap(byte[] bytes, int imageFormat, int width, int height) {
        return either(() -> {
            if (!rsAvailable) throw new Exception("RenderScript is not available");
            if (imageFormat != ImageFormat.NV21) throw new Exception("Library cannot be used");
            return Nv21Image.nv21ToBitmap(rs, bytes, width, height);
        }, () -> {
            Log.w("RS", "Could not use RenderScript, falling back to Java solution.");
            YuvImage yuv = new YuvImage(bytes, imageFormat, width, height, null);
            PipedInputStream in = new PipedInputStream();
            final PipedOutputStream out = new PipedOutputStream(in);
            yuv.compressToJpeg(new Rect(0, 0, width, height), 30, out);
            return BitmapFactory.decodeStream(in);
        });
    }

    public void stopCapture() {
        capture.set(false);
    }

    public boolean isFound() {
        return foundOne!=null;
    }

    public String getRaw() {
        return foundOne.rawValue;
    }

    public void release() {
        stopPreview();
        camera.release();
        camera = null;
        rs.destroy();
    }
}
