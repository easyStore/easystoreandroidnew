package hu.kleatech.easystorenew.service;

import java.util.List;
import java.util.Map;

import hu.kleatech.easystorenew.model.Element;
import hu.kleatech.easystorenew.model.ValidationError;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ElementService {
    @POST("elem/add")
    Call<List<ValidationError>> add(@Body Element elem);

    @POST("elem/save")
    Call<List<ValidationError>> save(@Body Element elem);

    @GET("elem/delete")
    Call<String> delete(@Query("compSerial") String compSerial, @Query("serial") String serial);

    @GET("elem")
    Call<Element> get(@Query("serial") String serial, @Query("compSerial") String compSerial);

    @GET("elem/all")
    Call<List<Element>> all(@Query("compSerial") String compSerial, @Query("page") int page, @Query("pageSize") int pageSize, @Query("requestImages") String requestImages);

    @GET("elem/genSerial")
    Call<String> genSerial(@Query("compSerial") String compSerial);

    @GET("elem/query")
    Call<List<Element>> search(@QueryMap Map<String, String> map);
}
