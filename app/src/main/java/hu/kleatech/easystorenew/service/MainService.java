package hu.kleatech.easystorenew.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MainService {
    @POST("test")
    Call<String> testConnection(@Body String str);
}
