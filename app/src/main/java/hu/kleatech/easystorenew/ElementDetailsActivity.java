package hu.kleatech.easystorenew;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.app.MainApplication;
import hu.kleatech.easystorenew.databinding.ActivityElementDetailsBinding;
import hu.kleatech.easystorenew.model.Element;
import hu.kleatech.easystorenew.model.ValidationError;
import hu.kleatech.easystorenew.service.ElementService;
import hu.kleatech.easystorenew.utils.Base64Utils;
import hu.kleatech.easystorenew.utils.Extensions;
import hu.kleatech.easystorenew.utils.GlobalState;
import hu.kleatech.easystorenew.utils.Utils;
import lombok.experimental.ExtensionMethod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.either;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;
import static hu.kleatech.easystorenew.utils.Utils.tryThis;

@ExtensionMethod(Extensions.class)
public class ElementDetailsActivity extends BaseActivity {

    public static final String SERIAL = "s";
    public static final String COMP_SERIAL = "c";
    public static final String EDITING = "e";
    private static final int CAMERA_RESULT = 3;

    @BindView(R.id.elementDetails_edittext_serial)  protected EditText serial;
    @BindView(R.id.elementDetails_edittext_name)    protected EditText name;

    private ActivityElementDetailsBinding binding;
    private ElementService elementService;
    private boolean editing;
    private Element element;
    private Unbinder unbinder;
    private boolean cameraOpen = false;
    private int hash;

    private final Helpers HELPERS = new Helpers();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_element_details);
        unbinder = ButterKnife.bind(this);
        editing = getIntent().getBooleanExtra(EDITING, false);
        nullsafe(() -> elementService = GlobalState.retrofit.create(ElementService.class));
        HELPERS.hideKeyboard();
        if(editing) {
            serial.setInputType(InputType.TYPE_NULL);
            serial.setFocusable(false);
            findViewById(R.id.elementDetails_button_gen).setVisibility(View.GONE);
            HELPERS.load();
        } else {
            binding.setElement(Element.newEmpty(getIntent().getStringExtra(COMP_SERIAL)));
            element = binding.getElement();
            hash = element.hashCode();
        }
    }

    @Override
    public void onBackPressed() {
        HELPERS.save();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!cameraOpen) finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    @OnClick(R.id.elementDetails_imageview_thumbnail)
    public void openCamera() {
        cameraOpen = true;
        GlobalState.imagesBase64 = element.getImagesBase64();
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra(CameraActivity.SERIAL, element.getSerial());
        intent.putExtra(CameraActivity.EDITING, editing);
        startActivityForResult(intent, CAMERA_RESULT);
    }

    @OnClick(R.id.elementDetails_fab_compartment)
    public void compartment() {
        HELPERS.save();
        Intent intent = new Intent(this, CompartmentDetailsActivity.class);
        intent.putExtra(CompartmentDetailsActivity.EDITING, true);
        intent.putExtra(CompartmentDetailsActivity.SERIAL, element.getCompartmentSerial());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RESULT) {
            if (resultCode == RESULT_OK) {
                element.setSerial(data.getStringExtra(SERIAL).or(serial.stringValue()));
                binding.elementDetailsTextviewNumberOfPictures.setText(
                        getString(R.string.layout_elementDetails_textview_text_noPicturesStored, element.getImagesBase64().size()));
                either(
                        () -> binding.elementDetailsImageviewThumbnail.setImageBitmap(Base64Utils.base64ToBitmap(element.getImagesBase64().get(0))),
                        () -> binding.elementDetailsImageviewThumbnail.setImageResource(android.R.drawable.ic_menu_gallery));
            }
            cameraOpen = false;
        }
    }

    @OnClick(R.id.elementDetails_fab_delete)
    public void delete() {
        if (!editing) {
            HELPERS.goBack();
            return;
        };
        showProgressBar(true);
        elementService.delete(element.getCompartmentSerial(), element.getSerial()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {
                    HELPERS.toast("Deleted");
                    finish();
                } else {
                    HELPERS.toast("Error while deleting: " + tryThis(() -> response.errorBody().string()).orElse(""));
                    showProgressBar(false);
                }
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                HELPERS.displayError(t);
                showProgressBar(false);
            }
        });
    }

    @OnClick(R.id.elementDetails_button_gen)
    public void generateSerial() {
        showProgressBar(true);
        elementService.genSerial(element.getCompartmentSerial()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) element.setSerial(response.body());
                else HELPERS.toast("ERROR: " + tryThis(() -> response.errorBody().string()).orElse(""));
                showProgressBar(false);
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("ERROR", Log.getStackTraceString(t));
                HELPERS.toast("ERROR: " + t.getMessage());
                showProgressBar(false);
            }
        });
    }

    //TODO refactor to central class
    private class Helpers {
        void hideKeyboard() {
            InputMethodManager imm = (InputMethodManager)ElementDetailsActivity.this.getSystemService(INPUT_METHOD_SERVICE);
            final Utils.Mutable<View> view = Mutable(ElementDetailsActivity.this.getCurrentFocus());
            if (view.obj != null) view.obj = new View(ElementDetailsActivity.this);
            nullsafe(() -> imm.hideSoftInputFromWindow(view.obj.getWindowToken(), 0));
        }
        //TODO refactor to central implementation
        void displayError(Throwable t) {
            //TODO dismiss progress bar
            Log.w("Compartments", t.getMessage());
            Toast.makeText(ElementDetailsActivity.this, "ERROR: " + t.getMessage(), Toast.LENGTH_LONG).show();

        }

        void toast(String s) {
            Toast.makeText(ElementDetailsActivity.this, s, Toast.LENGTH_LONG).show();
        }

        private void load() {
            showProgressBar(true);
            elementService.get(getIntent().getStringExtra(SERIAL), getIntent().getStringExtra(COMP_SERIAL))
                    .enqueue(new Callback<Element>() {
                        @Override
                        public void onResponse(@NonNull Call<Element> call, @NonNull Response<Element> response) {
                            Element element = response.body();
                            binding.setElement(element);
                            ElementDetailsActivity.this.element = binding.getElement();
                            hash = ElementDetailsActivity.this.element.hashCode();
                            showProgressBar(false);
                        }
                        @Override
                        public void onFailure(@NonNull Call<Element> call, @NonNull Throwable t) {
                            displayError(t);
                            binding.setElement(Element.newEmpty(getIntent().getStringExtra(COMP_SERIAL)));
                            ElementDetailsActivity.this.element = binding.getElement();
                            showProgressBar(false);
                        }
                    });
        }

        private void save() {
            if (hash == binding.getElement().hashCode()) {
                goBack();
                return;
            }
            showProgressBar(true);
            Element elem = binding.getElement();
            if(editing) elementService.save(elem).enqueue(new Callback<List<ValidationError>>() {
                @Override
                public void onResponse(@NonNull Call<List<ValidationError>> call, @NonNull Response<List<ValidationError>> response) {
                    if (response.isSuccessful()) {
                        toast("Saved");
                        goBack();
                        return;
                    } else {
                        for (ValidationError err : tryThis(() -> toValidationErrors(response.errorBody().string())).orElse(Collections.emptyList())) {
                            switch (err.getField()) {
                                case "serial" : serial.post(() -> serial.setError(err.getMessage())); break;
                                case "name" : name.post(() -> name.setError(err.getMessage())); break;
                                default : toast(err.getField() + ": " + err.getMessage());
                            }
                        }
                        showProgressBar(false);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<List<ValidationError>> call, @NonNull Throwable t) {
                    displayError(t);
                    goBack();
                    return;
                }
            });
            else elementService.add(elem).enqueue(new Callback<List<ValidationError>>() {
                  @Override
                  public void onResponse(@NonNull Call<List<ValidationError>> call, @NonNull Response<List<ValidationError>> response) {
                      if (response.isSuccessful()) {
                          toast("Saved");
                          goBack();
                          return;
                      } else {
                          for (ValidationError err : tryThis(() -> toValidationErrors(response.errorBody().string())).orElse(Collections.emptyList())) {
                              switch (err.getField()) {
                                  case "serial" : serial.post(() -> serial.setError(err.getMessage())); break;
                                  case "name" : name.post(() -> name.setError(err.getMessage())); break;
                                  default : toast(err.getField() + ": " + err.getMessage());
                              }
                          }
                          showProgressBar(false);
                      }
                  }
                  @Override
                  public void onFailure(@NonNull Call<List<ValidationError>> call, @NonNull Throwable t) {
                      displayError(t);
                      goBack();
                      return;
                  }
              }
            );
        }

        private void goBack() {
            ElementDetailsActivity.super.onBackPressed();
        }

        private List<ValidationError> toValidationErrors(String response) throws IOException {
            return new ObjectMapper().readValue(response, new TypeReference<List<ValidationError>>(){});
        }
    }
}
