package hu.kleatech.easystorenew.utils;

import android.util.Log;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

    public static <T> Else<T> tryThis(ProblematicSupplier<T> method) {
        try {
            T temp = method.call();
            return (t) -> temp;
        } catch (Exception e) {
            Log.w("tryThis catched", e);
            return (t) -> t;
        }
    }

    public static <S, A> Else<S> tryThis(A obj, ProblematicSupplierWithArgument<S, A> method) {
        try {
            S temp = method.call(obj);
            return t -> temp;
        } catch (Exception e) {
            Log.w("tryThis catched", e);
            return t -> t;
        }
    }

    public static Catch<Exception> tryThis(Command command) {
        try {
            command.execute();
            return co -> {
            };
        } catch (Exception ex) {
            return co -> co.execute(ex);
        }
    }

    public static void nullsafe(Command command) {
        try {
            command.execute();
        } catch (NullPointerException e) {
            Log.d("Ignored NullpointerEx", command.getClass().getName() + " was null.");
        } catch (Exception e) {
            Log.w("Ignored Ex", "Nullsafe method catched an unexpected exception.", e);
        }
    }

    public static void ignoreEx(Command command) {
        try {
            command.execute();
        } catch (Exception e) {
            Log.w("Ignored Ex", "IgnoreEx method cathced an exception: " + e.getMessage(), e);
        }
    }

    public static <T> void ignoreEx(T obj, CommandWithArgument<T> command) {
        try {
            command.execute(obj);
        } catch (Exception e) {
            Log.w("Ignored Ex", "IgnoreEx method cathced an exception: " + e.getMessage(), e);
        }
    }

    public static void delay(int ms) {
        ignoreEx(() -> Thread.sleep(ms));
    }

    public static Thread async(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
        return thread;
    }

    public static <T> T poll(Supplier<T> method, T notReadyMarker, int ms, int... limitDefault5000ms) {
        int limit = limitDefault5000ms.length == 0 ? 5000 : limitDefault5000ms[0];
        while (limit > 0) {
            limit -= ms;
            Log.d("Utils", "Polling...");
            T actual = method.call();
            if (actual == null) {
                if (notReadyMarker == null) delay(ms);
                else return null;
            } else if (actual.equals(notReadyMarker)) {
                delay(ms);
            } else return actual;
        }
        throw new RuntimeException("Polling limit exceeded");
    }

    @SafeVarargs
    public static <T> T print(T... obj) {
        StringBuilder builder = new StringBuilder();
        for (Object o : obj) {
            builder.append(String.valueOf(o));
            builder.append(' ');
        }
        Log.i("INFO", builder.toString());
        return obj[0];
    }

    public static <T> T either(ProblematicSupplier<T> first, ProblematicSupplier<T> second) {
        try {
            try {
                return first.call();
            }
            catch (Exception e) {
                return second.call();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void either(Command first, Command second) {
        try {
            first.execute();
        }
        catch (Exception e) {
            try {
                second.execute();
            }
            catch (Exception ex) {
                Log.w("Either catched", ex);
            }
        }
    }

    @FunctionalInterface
    public interface SimpleCommandWithArgument<T> {
        void execute(T obj);
    }

    @FunctionalInterface
    public interface Command {
        void execute() throws Exception;
    }

    @FunctionalInterface
    public interface CommandWithArgument<T> {
        void execute(T obj) throws Exception;
    }

    @FunctionalInterface
    public interface Supplier<T> {
        T call();
    }

    @FunctionalInterface
    public interface ProblematicSupplier<T> {
        T call() throws Exception;
    }

    @FunctionalInterface
    public interface ProblematicSupplierWithArgument<S, A> {
        S call(A obj) throws Exception;
    }

    @FunctionalInterface
    public interface Else<T> {
        T orElse(T t);
    }

    @FunctionalInterface
    public interface Catch<E extends Exception> {
        void orElse(SimpleCommandWithArgument<Exception> command);
        default void orNot() {}
    }

    public static class PlaceholderException extends Exception {
    }

    public static String url(String url) {
        if(!url.startsWith("http")) url = "http://" + url;
        return url;
    }

    public static class Mutable<T> {
        public T obj;
        public Mutable(T obj) {
            this.obj = obj;
        }
        public static <T> Mutable<T> Mutable(T obj) {
            return new Mutable<>(obj);
        }
    }
}
