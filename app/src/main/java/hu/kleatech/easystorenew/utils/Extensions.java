package hu.kleatech.easystorenew.utils;

import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.common.util.Strings;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import java.io.InputStream;
import java.io.InputStreamReader;

import lombok.experimental.UtilityClass;

import static hu.kleatech.easystorenew.utils.Utils.tryThis;

@UtilityClass
public class Extensions {
    public static String stringValue(Object o) {
        if (o==null)                    return "null";
        if (o instanceof InputStream)   return tryThis(() -> CharStreams.toString(new InputStreamReader((InputStream)o, Charsets.UTF_8))).orElse("exception");
        if (o instanceof EditText)      return ((EditText) o).getText().toString();
        return o.toString();
    }
    public static String or(String s, String ifNullOrWhitespace) {
        if (Strings.isEmptyOrWhitespace(s)) return ifNullOrWhitespace;
        return s;
    }
    @SuppressWarnings("unchecked")
    public static <T> T or(T obj, T insteadNull) {
        if (obj instanceof String) return (T)or((String)obj, (String)insteadNull);
        if (obj == null) return insteadNull;
        return obj;
    }
    public static <T> T print(T o) {
        Log.i("INFO", stringValue(o));
        return o;
    }
    public static <T> T print(T o, String msg) {
        Log.i("INFO", msg + stringValue(o));
        return o;
    }
}
