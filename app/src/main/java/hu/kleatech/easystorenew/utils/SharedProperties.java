package hu.kleatech.easystorenew.utils;

import android.annotation.SuppressLint;
import android.content.Context;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SharedProperties {

    @SuppressLint("StaticFieldLeak")
    private static Context context = null;

    /**Call this first.
     param context The context of the Launch activity. Else it will lead to memory leak.*/
    public static void setContext(Context context) {
        SharedProperties.context = context.getApplicationContext();
    }

    public static String getProperty(String key) {
        if (context == null) throw new IllegalStateException("Context is not initialized.");
        return context.getSharedPreferences("hu.kleatech.easystore", Context.MODE_PRIVATE).getString(key, "");
    }

    public static void setProperty(String key, String value) {
        if (context == null) throw new IllegalStateException("Context is not initialized.");
        context.getSharedPreferences("hu.kleatech.easystore", Context.MODE_PRIVATE).edit().putString(key, value).apply();
    }
}