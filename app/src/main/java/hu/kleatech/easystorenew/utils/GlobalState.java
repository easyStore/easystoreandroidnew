package hu.kleatech.easystorenew.utils;

import java.util.LinkedList;
import java.util.List;

import lombok.experimental.UtilityClass;
import retrofit2.Retrofit;

@UtilityClass
public class GlobalState {
    public static volatile Retrofit retrofit;
    public static volatile List<String> imagesBase64 = new LinkedList<>();
}
