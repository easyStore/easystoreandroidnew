package hu.kleatech.easystorenew;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.ArrayMap;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.databinding.ActivityCameraBinding;
import hu.kleatech.easystorenew.service.QRCamera;
import hu.kleatech.easystorenew.utils.ActivityWithMessageSupport;
import hu.kleatech.easystorenew.utils.Base64Utils;
import hu.kleatech.easystorenew.utils.GlobalState;

import static hu.kleatech.easystorenew.utils.Utils.async;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;
import static hu.kleatech.easystorenew.utils.Utils.tryThis;

public class CameraActivity extends BaseActivity implements ActivityWithMessageSupport {

    public static final String SERIAL = "s";
    public static final String EDITING = "e";
    private static final int PERMISSION_REQUEST = 2;

    @BindView(R.id.camera_gallery)              protected LinearLayout gallery;
    @BindView(R.id.camera_surfaceview)          protected TextureView surfaceView;
    @BindView(R.id.camera_textview_qr)          protected TextView qr;
    @BindView(R.id.camera_button_saveSerial)    protected Button saveSerial;
    @BindView(R.id.camera_linearlayout)         protected LinearLayout linearLayout;

    private Unbinder unbinder;
    private QRCamera qrCamera;
    private final ArrayMap<View, Bitmap> images = new ArrayMap<>(3);
    private boolean editing;
    ActivityCameraBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgressBar(true);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camera);
        unbinder = ButterKnife.bind(this);
        editing = getIntent().getBooleanExtra(EDITING, false);
        saveSerial.setEnabled(!editing);
        if (getCameraPermission()) init();
    }

    private void init() {
        async(() -> {
            qrCamera = tryThis(() -> new QRCamera(surfaceView, this)).orElse(null);
            if (qrCamera == null) {
                Toast.makeText(this, R.string.activity_camera_toast_qrCameraNotWorking, Toast.LENGTH_LONG).show();
                finish();
            }
            qrCamera.setOnFoundOneListener(() -> qr.setText(qrCamera.getRaw()));
            if (editing) {
                qr.post(() -> qr.setText(getIntent().getStringExtra(SERIAL)));
                runOnUiThread(() -> {
                    for (String img : GlobalState.imagesBase64) {
                        Bitmap bitmap = Base64Utils.base64ToBitmap(img);
                        View imageView = generateImageView(bitmap);
                        images.put(imageView, bitmap);
                        gallery.addView(imageView);
                        imageView.setOnClickListener((i) -> {
                            images.remove(i);
                            ((ViewManager)i.getParent()).removeView(i);
                        });
                    }
                });
            }
            qrCamera.startPreview();
            if (!editing) async(() -> qrCamera.startCapture(500));
            showProgressBar(false);
        });
    }

    private boolean getCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Snackbar.make(linearLayout, R.string.activity_camera_snackbar_cameraPermissionNeeded,
                        Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok,
                        view -> ActivityCompat.requestPermissions(CameraActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                PERMISSION_REQUEST)).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
            }
        } else
            return true;
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                init();
            } else {
                Toast.makeText(this, R.string.activity_camera_toast_dontHavePermission, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        nullsafe(() -> qrCamera.stopPreview());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getCameraPermission()) {
            nullsafe(() -> {
                qrCamera.startPreview();
                if (!editing) async(() -> qrCamera.startCapture(500));
            });
        }
    }

    @Override
    public void onBackPressed() {
        showProgressBar(true);
        GlobalState.imagesBase64.clear();
        for(int i=0; i<images.size(); i++) {
            GlobalState.imagesBase64.add(Base64Utils.bitmapToBase64(images.valueAt(i)));
            ((ViewManager)images.keyAt(i).getParent()).removeView(images.keyAt(i));
        }
        images.clear();
        Intent intent = new Intent();
        if (qrCamera.isFound()) intent.putExtra(SERIAL, qr.getText());
        setResult(qr.getText()==null ? RESULT_CANCELED : RESULT_OK, intent);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> qrCamera.release()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    @OnClick(R.id.camera_button_flash)
    public void turnOnFlash() {
        qrCamera.turnOnFlash();
    }

    @OnClick(R.id.camera_button_takePhoto)
    public void takePhoto() {
        showProgressBar(true);
        qrCamera.takeSnapshot(bm -> {
            View imageView = generateImageView(bm);
            images.put(imageView, bm);
            imageView.setOnClickListener(img -> {
                images.remove(img);
                ((ViewManager)img.getParent()).removeView(img);
            });
            gallery.post(() -> gallery.addView(imageView));
            showProgressBar(false);
        });
    }

    @OnClick(R.id.camera_button_saveSerial)
    public void saveSerial() {
        if (qrCamera.isFound()) {
            qrCamera.stopCapture();
            qr.setText(qrCamera.getRaw());
        }
    }

    private View generateImageView(Bitmap bitmap) {
        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageBitmap(bitmap);
        return imageView;
    }
}
