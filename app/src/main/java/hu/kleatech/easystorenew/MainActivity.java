package hu.kleatech.easystorenew;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.databinding.ActivityMainBinding;
import hu.kleatech.easystorenew.service.MainService;
import hu.kleatech.easystorenew.utils.Extensions;
import hu.kleatech.easystorenew.utils.GlobalState;
import hu.kleatech.easystorenew.utils.SharedProperties;
import lombok.experimental.ExtensionMethod;
import lombok.val;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static hu.kleatech.easystorenew.utils.Utils.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;
import static hu.kleatech.easystorenew.utils.Utils.tryThis;
import static hu.kleatech.easystorenew.utils.Utils.url;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

@ExtensionMethod(Extensions.class)
public class MainActivity extends BaseActivity {

    @BindView(R.id.main_editText_serverAddress) protected EditText serverAddress;
    @BindView(R.id.main_textView_serverStatus)  protected TextView serverStatus;
    @BindView(R.id.main_button_save)            protected Button save;
    @BindView(R.id.main_button_compartments)    protected Button compartments;

    private MainService mainService;
    private ActivityMainBinding binding;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        SharedProperties.setContext(this);
        serverAddress.setText(SharedProperties.getProperty("server-address"));
        serverStatus.setText(R.string.activity_main_serverStatus_text_checkingServerStatus);

        nullsafe(this::testConnection);
    }

    private boolean setService() {
        Retrofit retrofit = tryThis(() -> new Retrofit.Builder()
                                .baseUrl(url(serverAddress.stringValue()))
                                .client(provideOkHttpClient())
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build())
                .orElse(null);
        GlobalState.retrofit = retrofit;
        final Mutable<Boolean> success = Mutable(true);
        tryThis(() -> { mainService = retrofit.create(MainService.class); }).orElse(ex -> success.obj = false);
        return success.obj;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    private OkHttpClient provideOkHttpClient() {
        val builder = new OkHttpClient.Builder();
        if (false && BuildConfig.DEBUG) {
            val httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    private void testConnection() {
        save.setEnabled(false);
        compartments.setEnabled(false);
        if (!setService()) return;
        mainService.testConnection("Testing").enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.body().or("").contains("Success")) {
                    serverStatus.post(() -> serverStatus.setText(R.string.activity_main_serverStatus_text_serverStatusUp));
                }
                save.setEnabled(true);
                compartments.setEnabled(true);
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                serverStatus.post(() -> serverStatus.setText(getString(R.string.activity_main_serverStatus_text_serverStatusDown) + t.getMessage()));
                save.setEnabled(true);
            }
        });
    }

    @OnClick(R.id.main_button_save)
    protected void testAndSave() {
        SharedProperties.setProperty("server-address", serverAddress.getText().toString());
        serverStatus.setText(getString(R.string.activity_main_serverStatus_text_checkingServerStatus));
        testConnection();
    }

    @OnClick(R.id.main_button_compartments)
    protected void viewCompartments() {
        Intent intent = new Intent(this, CompartmentsActivity.class);
        startActivity(intent);
    }

}
