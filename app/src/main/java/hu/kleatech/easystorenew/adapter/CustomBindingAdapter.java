package hu.kleatech.easystorenew.adapter;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.text.TextUtils;
import android.widget.*;

import hu.kleatech.easystorenew.utils.Base64Utils;
import hu.kleatech.easystorenew.utils.Extensions;
import lombok.experimental.ExtensionMethod;
import lombok.experimental.UtilityClass;

import static hu.kleatech.easystorenew.utils.Utils.nullsafe;

@UtilityClass
@ExtensionMethod(Extensions.class)
public class CustomBindingAdapter {
    @BindingAdapter({"imageBase64"})
    public static void loadImage(ImageView imageView, String base64) {
        if (TextUtils.isEmpty(base64)) {
            imageView.setImageResource(android.R.drawable.ic_menu_gallery);
            return;
        }
        imageView.setImageBitmap(Base64Utils.base64ToBitmap(base64));
    }
}
