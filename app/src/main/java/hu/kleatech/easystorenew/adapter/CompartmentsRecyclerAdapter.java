package hu.kleatech.easystorenew.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hu.kleatech.easystorenew.BR;
import hu.kleatech.easystorenew.R;
import hu.kleatech.easystorenew.model.Compartment;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString
@AllArgsConstructor
public class CompartmentsRecyclerAdapter extends RecyclerView.Adapter<CompartmentsRecyclerAdapter.BindingHolder> {

    private List<Compartment> compartments;

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.compartment_row, parent, false);
        return new BindingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        holder.getBinding().setVariable(BR.Compartment, compartments.get(position));
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return compartments==null?0:compartments.size();
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        BindingHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        ViewDataBinding getBinding() {
            return binding;
        }
    }
}
