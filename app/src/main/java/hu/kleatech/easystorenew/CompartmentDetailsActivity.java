package hu.kleatech.easystorenew;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import hu.kleatech.easystorenew.app.MainApplication;
import hu.kleatech.easystorenew.databinding.ActivityCompartmentDetailsBinding;
import hu.kleatech.easystorenew.model.Compartment;
import hu.kleatech.easystorenew.model.ValidationError;
import hu.kleatech.easystorenew.service.CompartmentService;
import hu.kleatech.easystorenew.utils.Base64Utils;
import hu.kleatech.easystorenew.utils.Extensions;
import hu.kleatech.easystorenew.utils.GlobalState;
import hu.kleatech.easystorenew.utils.Utils;
import lombok.experimental.ExtensionMethod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static hu.kleatech.easystorenew.utils.Utils.Mutable.Mutable;
import static hu.kleatech.easystorenew.utils.Utils.either;
import static hu.kleatech.easystorenew.utils.Utils.nullsafe;
import static hu.kleatech.easystorenew.utils.Utils.tryThis;

@ExtensionMethod(Extensions.class)
public class CompartmentDetailsActivity extends BaseActivity {

    public static final String SERIAL = "s";
    public static final String EDITING = "e";
    private static final int CAMERA_RESULT = 1;

    @BindView(R.id.compartmentDetails_edittext_serial)  protected EditText serial;
    @BindView(R.id.compartmentDetails_edittext_name)    protected EditText name;

    private ActivityCompartmentDetailsBinding binding;
    private CompartmentService compartmentService;
    private boolean editing;
    private Compartment compartment;
    private Unbinder unbinder;
    private boolean cameraOpen = false;
    private int hash;

    private final Helpers HELPERS = new Helpers();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_compartment_details);
        unbinder = ButterKnife.bind(this);
        editing = getIntent().getBooleanExtra(EDITING, false);
        nullsafe(() -> compartmentService = GlobalState.retrofit.create(CompartmentService.class));
        HELPERS.hideKeyboard();
        if(editing) {
            serial.setInputType(InputType.TYPE_NULL);
            serial.setFocusable(false);
            HELPERS.load();
        } else {
            binding.setCompartment(Compartment.newEmpty());
            compartment = binding.getCompartment();
            hash = compartment.hashCode();
        }
    }

    @Override
    public void onBackPressed() {
        HELPERS.save(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!cameraOpen) finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tryThis(() -> unbinder.unbind()).orNot();
        tryThis(() -> binding.unbind()).orNot();
    }

    @OnClick(R.id.compartmentDetails_imageview_thumbnail)
    public void openCamera() {
        cameraOpen = true;
        GlobalState.imagesBase64 = compartment.getImagesBase64();
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra(CameraActivity.SERIAL, compartment.getSerial());
        intent.putExtra(CameraActivity.EDITING, editing);
        startActivityForResult(intent, CAMERA_RESULT);
    }

    @OnClick(R.id.compartmentDetails_fab_elements)
    public void elements() {
        HELPERS.save(false);
        Intent intent = new Intent(this, ElementsActivity.class);
        intent.putExtra(ElementsActivity.HAVE_CONTEXT, true);
        intent.putExtra(ElementsActivity.COMP_SERIAL, compartment.getSerial());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RESULT) {
            if (resultCode == RESULT_OK) {
                compartment.setSerial(data.getStringExtra(SERIAL).or(serial.stringValue()));
                binding.compartmentDetailsTextviewNumberOfPictures.setText(
                    getString(R.string.layout_compartmentDetails_textview_text_noPicturesStored, compartment.getImagesBase64().size()));
                either(
                    () -> binding.compartmentDetailsImageviewThumbnail.setImageBitmap(Base64Utils.base64ToBitmap(compartment.getImagesBase64().get(0))),
                    () -> binding.compartmentDetailsImageviewThumbnail.setImageResource(android.R.drawable.ic_menu_gallery));
            }
            cameraOpen = false;
        }
    }

    @OnClick(R.id.compartmentDetails_fab_delete)
    public void delete() {
        if (!editing) {
            HELPERS.goBack();
            return;
        };
        showProgressBar(true);
        compartmentService.delete(compartment.getSerial()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {
                    HELPERS.toast("Deleted");
                    HELPERS.goBack();
                    return;
                } else {
                    HELPERS.toast("Error while deleting: " + tryThis(() -> response.errorBody().string()).orElse(""));
                    showProgressBar(false);
                }
            }
            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                HELPERS.displayError(t);
                HELPERS.goBack();
                return;
            }
        });
    }

    //TODO refactor to central class
    private class Helpers {
        void hideKeyboard() {
            InputMethodManager imm = (InputMethodManager)CompartmentDetailsActivity.this.getSystemService(INPUT_METHOD_SERVICE);
            final Utils.Mutable<View> view = Mutable(CompartmentDetailsActivity.this.getCurrentFocus());
            if (view.obj != null) view.obj = new View(CompartmentDetailsActivity.this);
            nullsafe(() -> imm.hideSoftInputFromWindow(view.obj.getWindowToken(), 0));
        }
        //TODO refactor to central implementation
        void displayError(Throwable t) {
            Log.w("Compartments", t.getMessage());
            Toast.makeText(CompartmentDetailsActivity.this, "ERROR: " + t.getMessage(), Toast.LENGTH_LONG).show();
        }

        void toast(String s) {
            Toast.makeText(CompartmentDetailsActivity.this, s, Toast.LENGTH_LONG).show();
        }

        private void load() {
            showProgressBar(true);
            compartmentService.get(getIntent().getStringExtra(SERIAL))
                    .enqueue(new Callback<Compartment>() {
                        @Override
                        public void onResponse(@NonNull Call<Compartment> call, @NonNull Response<Compartment> response) {
                            Compartment compartment = response.body();
                            binding.setCompartment(compartment);
                            CompartmentDetailsActivity.this.compartment = binding.getCompartment();
                            hash = CompartmentDetailsActivity.this.compartment.hashCode();
                            showProgressBar(false);
                        }
                        @Override
                        public void onFailure(@NonNull Call<Compartment> call, @NonNull Throwable t) {
                            displayError(t);
                            binding.setCompartment(Compartment.newEmpty());
                            CompartmentDetailsActivity.this.compartment = binding.getCompartment();
                            showProgressBar(false);
                        }
                    });
        }

        private void save(boolean backPressed) {
            if (hash == binding.getCompartment().hashCode()) {
                goBack();
                return;
            }
            showProgressBar(true);
            Compartment comp = binding.getCompartment();
            if(editing) compartmentService.save(comp).enqueue(new Callback<List<ValidationError>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<ValidationError>> call, @NonNull Response<List<ValidationError>> response) {
                            if (response.isSuccessful()) {
                                toast("Saved");
                                if (backPressed) {
                                    goBack();
                                    return;
                                }
                            } else {
                                for (ValidationError err : tryThis(() -> toValidationErrors(response.errorBody().string())).orElse(Collections.emptyList())) {
                                    switch (err.getField()) {
                                        case "serial" : serial.post(() -> serial.setError(err.getMessage())); break;
                                        case "name" : name.post(() -> name.setError(err.getMessage())); break;
                                        default : toast(err.getField() + ": " + err.getMessage());
                                    }
                                }
                                showProgressBar(false);
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<List<ValidationError>> call, @NonNull Throwable t) {
                            displayError(t);
                            goBack();
                            return;
                        }
                    });
            else compartmentService.add(comp).enqueue(new Callback<List<ValidationError>>() {
                       @Override
                       public void onResponse(@NonNull Call<List<ValidationError>> call, @NonNull Response<List<ValidationError>> response) {
                           if (response.isSuccessful()) {
                               toast("Saved");
                               CompartmentDetailsActivity.super.onBackPressed();
                           } else {
                               for (ValidationError err : tryThis(() -> toValidationErrors(response.errorBody().string())).orElse(Collections.emptyList())) {
                                   switch (err.getField()) {
                                       case "serial" : serial.post(() -> serial.setError(err.getMessage())); break;
                                       case "name" : name.post(() -> name.setError(err.getMessage())); break;
                                       default : toast(err.getField() + ": " + err.getMessage());
                                   }
                               }
                               showProgressBar(false);
                           }
                       }
                       @Override
                       public void onFailure(@NonNull Call<List<ValidationError>> call, @NonNull Throwable t) {
                           displayError(t);
                           goBack();
                           return;
                       }
                   }
            );
        }

        private void goBack() {
            Intent intent = new Intent(CompartmentDetailsActivity.this, CompartmentsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        private List<ValidationError> toValidationErrors(String response) throws IOException {
            return new ObjectMapper().readValue(response, new TypeReference<List<ValidationError>>(){});
        }
    }
}
